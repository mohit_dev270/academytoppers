<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PagesModel extends CI_Model
{
    public $table = 'pages';  
 
    public function selectAll()
    {
       return $this->db->get($this->table)->result_array();

    }
    public function insert($data)
    {
       return $this->db->insert($this->table,$data);
    }
    public function dataUpdate($data,$where)
    {
       $this->db->where($where);
       return $this->db->update($this->table,$data);
    }
    public function slectDataUseWhere($where)
    {
       return $this->db->get_where($this->table,$where)->result_array();

    }
    public function slectDataUseWhereSingleRow($where)
    {
       return $this->db->get_where($this->table,$where)->row_array();

    }
    

}

?>