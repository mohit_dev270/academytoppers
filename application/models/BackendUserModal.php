<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BackendUserModal extends CI_Model
{
  public function selectUsersWhere($where)
  {
    $query = $this->db->get_where('backend_users',$where)->row_array();
    return $query;
  }

}

?>