<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseModel extends CI_Model
{
    public $table = 'course';  
 
    public function getAllCourse()
    {
       return $this->db->get($this->table)->result_array();

    }
    public function courseUpdate($where,$updateData)
    {
        $this->db->where($where);
       return $this->db->update($this->table,$updateData);

    }
    public function courseInsert($insertData)
    {

       return $this->db->Insert($this->table,$insertData);

    }
    public function getCourseWhere($where)
    {
       return $this->db->get_where($this->table,$where)->row_array();

    }

}

?>