<?php

defined('BASEPATH') or exit('No direct script access allowed');



class FrontendAuth extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();

		
        $this->load->model('FrontendUserModal');

		if (!empty($this->session->userdata('logged_student'))) {

			redirect(site_url('studentsauth/Student'));
		}
	}

	public function index()

	{

		$this->load->view('acadmytoper/comman/header');
		$this->load->view('acadmytoper/Homepage');
		$this->load->view('acadmytoper/comman/footer');
	}
	
	public function studentRegister()

	{
		$this->form_validation->set_rules('firstname', 'firstname', 'trim|required');
		$this->form_validation->set_rules('lastname', 'lastname', 'trim|required');
		$this->form_validation->set_rules('mobile', 'mobile', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required|is_unique[students.email]');
		$this->form_validation->set_rules('state', 'state', 'trim|required');
		$this->form_validation->set_rules('city', 'city', 'trim|required');
		$this->form_validation->set_rules('pincode', 'pincode', 'trim|required');
		if ($this->form_validation->run() == TRUE) 
		{
			$token = $this->tokenGenrater();
			$data = array(
				
				'firstname' => $this->input->post('firstname', TRUE),
				'lastname' => $this->input->post('lastname', TRUE),
				'mobile' => $this->input->post('mobile', TRUE),
		    	'email' => $this->input->post('email', TRUE),
				'state' => $this->input->post('state', TRUE),
				'city' => $this->input->post('city', TRUE),
				'pincode' => $this->input->post('pincode', TRUE),
				'token' => $token
			);
			$userName =  $this->input->post('firstname', TRUE);
			$response = $this->FrontendUserModal->insert($data);
			if($response)
			{	
			
		
			$to =$this->input->post('email');
			$from = $this->input->post('email');
			$subject = 	'Verification Link for Topers Acadmey';
	        $message = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
			<div class="email-formet">
			<h1 style="color:#eb1616;">Confirm your email address</h1></br>
			<p>Thanks for joining Topers Acadmey!</p> </br> <p>We need to check if this is right email address so we can send you important information and updates.</p> </br> </br>
				<a href="' . base_url() . 'emailverification/' . $token . '" style="background-color:#eb1616; border:1px solid #eb1616; border-color:#eb1616; border-radius:0px; border-width:1px; color:#ffff; display:inline-block; font-size:14px; font-weight:normal; letter-spacing:0px; line-height:normal; padding:12px 40px 12px 40px; text-align:center; text-decoration:none; border-style:solid; font-family:inherit;">Confirm email address</a></br></br>
				<p>You are receiving this email because you have registered on Topers Acadmey.</p>
			</br></br></div>';
			$this->email->set_newline("\r\n");
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($message);
	
			if ($this->email->send()) {
				$this->session->set_flashdata('successMsg', 'Verify Your Email and login to continue..');
				redirect(site_url());
			} else {
				$this->session->set_flashdata('error', 'Email is not Correct');
				redirect(site_url());
			}
			}
			redirect(site_url());
		}
		else
		{
			$errors = validation_errors();
			$this->session->set_flashdata('error', $errors);
			redirect(base_url());
		}
	}
	public function tokenGenrater()
	{
		$token = bin2hex(random_bytes(20));
		return $token;
	}
	public function emailVerification(){
		$where = array('token' => $this->uri->segment(2));
        $token = $this->FrontendUserModal->slectDataWhere($where);
		if(!empty($token))
		{
			$this->session->set_flashdata('successMsg', 'Email verified. Now you can login.');
            redirect(base_url());
		}
		else
		{
			$this->session->set_flashdata('successMsg', 'Link is expire');
            redirect(base_url());
		}


	}	

}
