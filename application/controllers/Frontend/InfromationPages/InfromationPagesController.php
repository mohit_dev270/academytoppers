<?php

defined('BASEPATH') or exit('No direct script access allowed');



class InfromationPagesController extends CI_Controller

{
	public function __construct()

	{
			parent::__construct();
			{
				$this->load->Model('IformationPagesModel');
			}
	}		
	public function index()

	{
		$pagesId = $this->uri->segment(2);
		$where = ['id'=>$pagesId];
		$pagesContent = $this->IformationPagesModel->slectDataUseWhereSingleRow($where);
		$this->load->view('acadmytoper/comman/header');
		$this->load->view('Frontend/InformationPages/InformationPages',compact('pagesContent'));
		$this->load->view('acadmytoper/comman/footer');
	}
	

}
