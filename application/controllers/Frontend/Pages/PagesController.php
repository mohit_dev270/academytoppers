<?php

defined('BASEPATH') or exit('No direct script access allowed');



class PagesController extends CI_Controller

{
	public function __construct()

	{
			parent::__construct();
			{
				$this->load->Model('PagesModel');
			}
	}		
	public function index()

	{
		$pageName = $this->uri->segment(2);

	
		$where = ['pageName'=>$pageName];
		$pagesContent = $this->PagesModel->slectDataUseWhereSingleRow($where);
		$this->load->view('acadmytoper/comman/header');
		$this->load->view('Frontend/Pages/Pages',compact('pagesContent'));
		$this->load->view('acadmytoper/comman/footer');
	}
	

}
