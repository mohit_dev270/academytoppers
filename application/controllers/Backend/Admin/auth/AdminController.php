<?php

defined('BASEPATH') or exit('No direct script access allowed');



class AdminController extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();
		{
			$this->load->model('BackendUserModal');
		}
	
	}

	public function index()

	{

		$this->load->view('Backend/Admin/auth/login');
		
    }
	public function adminVerify()

	{
		$where = array('status'=>0,'username'=>$this->input->post('username'),'password'=>md5($this->input->post('password')));	

		$users = $this->BackendUserModal->selectUsersWhere($where);
		if(!empty($users))
		{
			if($users['role']== 'Admin')
			{
				$this->session->set_userdata('loogedinBackend', $users);
			
				redirect('Admin/dashborad');
			}
		}
		else
		{
			redirect('Admin');
		}


	}

}
