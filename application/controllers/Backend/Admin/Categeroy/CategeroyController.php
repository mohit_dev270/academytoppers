<?php

defined('BASEPATH') or exit('No direct script access allowed');



class CategeroyController extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();
		{
			$user = $this->session->userdata('loogedinBackend');
			if(empty($user))
			{
	      			redirect('Admin');
			}
			if($user['role'] != 'Admin')
			{
					redirect('Admin');
			}
			$this->load->Model('CategeryModel');

		}
	
	}

	public function courseUpdate()
	{
			$courseId = $this->uri->segment(3);
			$courseData = $this->CourseModel->getCourseWhere(array('id'=>$courseId));	
			
		
			$this->load->view('Backend/Admin/Comman/header');
			$this->load->view('Backend/Admin/Course/CourseForm',compact('courseData'));
			$this->load->view('Backend/Admin/Comman/footer');
			if(isset($_POST['submit']))
			{
				$updateData = array();
				$updateData['tittle'] = $this->input->post('tittle');
				$updateData['short_description'] = $this->input->post('shortDescription');
				$updateData['description'] = $this->input->post('description');
				$updateData['price'] = $this->input->post('price');
				if(!empty($_FILES['cover_image']['name']))
				{
					$config['upload_path'] = 'Images/courseimage';
					$config['allowed_types'] = 'gif|jpg|png';
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('cover_image'))
						
					{
							$error = array('error' => $this->upload->display_errors());
							print_r($error);
					}
					else
					{
							$data = $this->upload->data();
							$updateData['cover_image']  = $data['file_name'];
					}
				}
			
				$where = array('id'=>$courseId);
				$this->CourseModel->courseUpdate($where,$updateData);
				redirect('Admin/coursesList');
			}
    }


	public function CategeroyInsert()
	{
			
	
			
				$insertData['categeryName'] = $this->input->post('catgeroyName');
			    $this->CategeryModel->Categeroyinsert($insertData);
				redirect('Admin/CategeroyList');
		
		
		

	}












}
