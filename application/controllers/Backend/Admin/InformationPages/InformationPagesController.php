<?php

defined('BASEPATH') or exit('No direct script access allowed');



class InformationPagesController extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();
		{
			$user = $this->session->userdata('loogedinBackend');
			if(empty($user))
			{
	      			redirect('Admin');
			}
			if($user['role'] != 'Admin')
			{
					redirect('Admin');
			}
			$this->load->Model('IformationPagesModel');

		}
	
	}
	public function informationpagesFormAdd()
	{
		$this->load->view('Backend/Admin/Comman/header');
		$this->load->view('Backend/Admin/iformationPages/InformationPagesListForm');
		$this->load->view('Backend/Admin/Comman/footer');
		if(isset($_POST['submit']))
		{
		
			$pageContent =['tittle'=>$this->input->post('tittle'),'pageContent'=>$this->input->post('pageContent'),'categeroyId'=>$this->input->post('categeroyId')];
			$this->IformationPagesModel->insert($pageContent);
			redirect('Admin/InformationPagesList');

		}
	}
	public function informationpagesFormUpdate()
	{
		$updateId = $this->uri->segment(3);
		$where = ['id'=>$updateId];

		$informationPagesData = $this->IformationPagesModel->slectDataUseWhereSingleRow($where);
	
		$this->load->view('Backend/Admin/Comman/header');
		$this->load->view('Backend/Admin/iformationPages/InformationPagesListForm',compact('informationPagesData'));
		$this->load->view('Backend/Admin/Comman/footer');
		if(isset($_POST['submit']))
		{
		
			$pageContent =['tittle'=>$this->input->post('tittle'),'pageContent'=>$this->input->post('pageContent'),'categeroyId'=>$this->input->post('categeroyId')];
			$this->IformationPagesModel->dataUpdate($pageContent,$where);
			redirect('Admin/InformationPagesList');

		}
	}
	
}







