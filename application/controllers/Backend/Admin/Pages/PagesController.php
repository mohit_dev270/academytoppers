<?php

defined('BASEPATH') or exit('No direct script access allowed');



class PagesController extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();
		{
			$user = $this->session->userdata('loogedinBackend');
			if(empty($user))
			{
	      			redirect('Admin');
			}
			if($user['role'] != 'Admin')
			{
					redirect('Admin');
			}
			$this->load->Model('PagesModel');

		}
	
	}
	public function pagesFormAdd()
	{

		$this->load->view('Backend/Admin/Comman/header');
		$this->load->view('Backend/Admin/Pages/pagesListForm');
		$this->load->view('Backend/Admin/Comman/footer');
		if(isset($_POST['submit']))
		{
		
			
			$pageContent =['pageName'=>$this->input->post('pageName'),'pageContent'=>$this->input->post('pageContent')];
			$this->PagesModel->insert($pageContent);
			redirect('Admin/PagesList');

		}
	}
	public function pagesFormUpdate()
	{
		$updateId = $this->uri->segment(3);
		$where = ['id'=>$updateId];
	
		$pagesData = $this->PagesModel->slectDataUseWhereSingleRow($where);
		
		$this->load->view('Backend/Admin/Comman/header');
		$this->load->view('Backend/Admin/Pages/pagesListForm',compact('pagesData'));
		$this->load->view('Backend/Admin/Comman/footer');
		if(isset($_POST['submit']))
		{
		
			$pageContent =['pageName'=>$this->input->post('pageName'),'pageContent'=>$this->input->post('pageContent')];
			$this->PagesModel->dataUpdate($pageContent,$where);
			redirect('Admin/PagesList');

		}
	}
	
}







