<?php

defined('BASEPATH') or exit('No direct script access allowed');



class AdminDashboradController extends CI_Controller

{

	public function __construct()

	{

		parent::__construct();
		{
			$user = $this->session->userdata('loogedinBackend');
			if(empty($user))
			{
	      			redirect('Admin');
			}
			if($user['role'] != 'Admin')
			{
					redirect('Admin');
			}
			$this->load->Model('CourseModel');
			$this->load->Model('StudentModel');
			$this->load->Model('CategeryModel');
			$this->load->Model('IformationPagesModel');
			$this->load->Model('PagesModel');

		}
	
	}

	public function index()
	{

			$this->load->view('Backend/Admin/Comman/header');
			$this->load->view('Backend/Admin/Dashborad/dashborad');
			$this->load->view('Backend/Admin/Comman/footer');
		
    }
	public function courseList()
	{

		$courseList = $this->CourseModel->getAllCourse();
		
		$this->load->view('Backend/Admin/Comman/header');
		$this->load->view('Backend/Admin/Course/CourseList',compact('courseList'));
		$this->load->view('Backend/Admin/Comman/footer');
	}
	public function studentList()
	{
		$studentList = $this->StudentModel->getAllStudent();

		$this->load->view('Backend/Admin/Comman/header');
		$this->load->view('Backend/Admin/Student/StudentList',compact('studentList'));
		$this->load->view('Backend/Admin/Comman/footer');
	}
	public function categeryList()
	{
		$categerys = $this->CategeryModel->selectAll();
		$this->load->view('Backend/Admin/Comman/header');
		$this->load->view('Backend/Admin/Categery/CategeryList',compact('categerys'));
		$this->load->view('Backend/Admin/Comman/footer');
	}
	public function informationpagesList()
	{
		$informationPagesLists = $this->IformationPagesModel->selectAll();
		$this->load->view('Backend/Admin/Comman/header');
		$this->load->view('Backend/Admin/iformationPages/InformationPagesList',compact('informationPagesLists'));
		$this->load->view('Backend/Admin/Comman/footer');
	}
	public function pagesList()
	{
		$pagesLists = $this->PagesModel->selectAll();
		$this->load->view('Backend/Admin/Comman/header');
		$this->load->view('Backend/Admin/pages/PagesList',compact('pagesLists'));
		$this->load->view('Backend/Admin/Comman/footer');
	}

}
