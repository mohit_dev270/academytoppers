<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'HomeController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['course'] = 'HomeController/course';
$route['register'] = 'Auth/FrontendAuth/studentRegister';
$route['emailverification/(:any)'] = 'Auth/FrontendAuth/emailVerification';
// Frontend information page
$route['informationPages/(:any)'] = 'Frontend/InfromationPages/InfromationPagesController';
// Frontend  pages
$route['pages/(:any)'] = 'Frontend/Pages/PagesController';



//Admin 
$route['Admin'] = 'Backend/Admin/auth/AdminController';
$route['Admin/auth'] = 'Backend/Admin/auth/AdminController/adminVerify';
//Admin Dashborad
$route['Admin/auth'] = 'Backend/Admin/auth/AdminController/adminVerify';
$route['Admin/dashborad'] = 'Backend/Admin/AdminDashborad/AdminDashboradController';
$route['Admin/coursesList'] = 'Backend/Admin/AdminDashborad/AdminDashboradController/courseList';
$route['Admin/coursesUpdate/(:num)'] = 'Backend/Admin/Course/CourseController/courseUpdate';
$route['Admin/courseinsert'] = 'Backend/Admin/Course/CourseController/courseInsert';
$route['Admin/studentsList'] = 'Backend/Admin/AdminDashborad/AdminDashboradController/studentList';


//Admin  Categeroy 
$route['Admin/CategeroyList'] = 'Backend/Admin/AdminDashborad/AdminDashboradController/categeryList';
$route['Admin/CategeroyInsert'] = 'Backend/Admin/Categeroy/CategeroyController/CategeroyInsert';



//Admin  Information Pages 
$route['Admin/InformationPagesList'] = 'Backend/Admin/AdminDashborad/AdminDashboradController/informationpagesList';
$route['Admin/InformationPagesAdd'] = 'Backend/Admin/InformationPages/InformationPagesController/informationpagesFormAdd';
$route['Admin/InformationPagesUpdate/(:num)'] = 'Backend/Admin/InformationPages/InformationPagesController/informationpagesFormUpdate';

//Admin  Pages 
$route['Admin/PagesList'] = 'Backend/Admin/AdminDashborad/AdminDashboradController/pagesList';
$route['Admin/PagesAdd'] = 'Backend/Admin/Pages/PagesController/pagesFormAdd';
$route['Admin/PagesUpdate/(:num)'] = 'Backend/Admin/Pages/PagesController/pagesFormUpdate';