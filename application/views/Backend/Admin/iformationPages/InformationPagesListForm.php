
<div class="main-panel">        
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
             
            <?= (!empty($informationPagesData['tittle']))? 'Information Page Update':'Information Page Insert';  ?>
          </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Information Page</a></li>
              
                <li class="breadcrumb-item active" aria-current="page"><?= (!empty($informationPagesData['tittle']))? 'Course Update':'Course Insert';  ?></li>
                </ol>
            </nav>
          </div>
          <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
              
                  <form class="forms-sample" method="POST" action="" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="exampleInputUsername1">Tittle</label>

                      <input type="text" name="tittle" class="form-control" value="<?= (!empty($informationPagesData['tittle']))? $informationPagesData['tittle']:'';  ?>" placeholder="Tittle" required>
                    </div>
                  
                  
                    <div class="form-group">
                      <label for="exampleInputUsername1">Select a Categeroy</label>

                        <select class="form-control" name="categeroyId" aria-label=".form-select-sm example" required>
                          <option selected>Select a Categeroy</option>
                          <?php foreach(getAllCategery() as $categery){ ?>
                         
                          <option value="<?=$categery['id']?>"   <?= (!empty($informationPagesData['categeroyId']) && $informationPagesData['categeroyId'] == $categery['id'])? 'selected':'';  ?>        ><?=$categery['categeryName']?></option>
                          <?php } ?>
                          
                        </select>  
                    </div>
                  
                  
                  
                  
                  
                  
                    <div class="form-group">
                      <label for="exampleInputEmail1">Page Content</label>
  
                      <textarea class="summernote form-control" rows="200" cols="100" name="pageContent"><?= (!empty($informationPagesData['pageContent']))? $informationPagesData['pageContent']:'';  ?></textarea>
                    </div>
      

                    <button type="submit" name="submit" class="btn btn-primary mr-2">Submit</button>
                  
                  </form>
                </div>
              </div>
            </div>
           
          </div>
        </div>
    