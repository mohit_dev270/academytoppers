<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
           Information Pages List
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Tables</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data table</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              <h4 class="card-title"> Information Pages List</h4>
              <div class="row">
                <div class="col-12">
                <a href="<?=site_url('Admin/InformationPagesAdd');?>" class="btn btn-outline-primary float-right mb-2">Cerate Page</a>
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Sr No #</th>
                            <th>Page Name</th>
                            <!-- <th>Price</th>
                            <th>Cover Image</th>
                            <th>Status</th>-->
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                     
                     <?php 
                     $no = 1;
                     foreach($informationPagesLists as $informationPagesList)
                     {
                     ?>
                      <tr>
                        <td><?=$no++;?>.</td>
                        <td><?=$informationPagesList['tittle']?></td>
                        <td><a class="btn btn-primary" href="<?=site_url('Admin/InformationPagesUpdate/').$informationPagesList['id'];?>">Update</a></td>
                        

                      </tr>
                  <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              </div>
  
        