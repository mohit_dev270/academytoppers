<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
            Category List
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Tables</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data table</li>
                <li class="breadcrumb-item active" aria-current="page">Data table</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Category List</h4>
              <div class="row">
                <div class="col-12">
                <button type="button" class="btn btn-primary float-right mb-2" data-toggle="modal" data-target="#categeoryModal">Insert Category </button>
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Sr No #</th>
                            <th> Name</th>
                            <th> Status</th>
                            <th> Action</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                     
                     <?php 
                     $no = 1;
                     foreach($categerys as $categery)
                     {
                     ?>
                      <tr>
                        <td><?=$no++;?>.</td>
                        <td><?=$categery['categeryName']?></td>
                      
                        <td> 
                      
                        <label class="badge badge-success"><?= ($categery['status'] == 0)? 'Active':'In-Active';  ?></label></td>
                      
                        <td> 
                      
                        <a class="btn btn-primary" href="#">Delete</a>
                        <a class="btn btn-primary" href="#">Update</a>
                        </td>
                      

                      </tr>
                  <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              </div>
  
              
              <!-- Modal -->
              <div class="modal fade" id="categeoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Categeroy</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="<?=site_url('Admin/CategeroyInsert')?>" method="POST">
            <div class="modal-body">

            <div class="form-group">
              <label for="email">Add Categeroy:</label>
              <input type="text" class="form-control" name="catgeroyName" placeholder="Add Categeroy">
            </div>
            
            
            
            
            
            
            </div>
            <div class="modal-footer">
               <input type="submit" class="btn btn-primary" name="submit"></input>
               <span  class="btn btn-secondary" data-dismiss="modal">Close</span>
            </div>
         </form>
      </div>
   </div>
</div>