        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <!-- <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2018 <a href="https://www.urbanui.com/" target="_blank">Urbanui</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="far fa-heart text-danger"></i></span> -->
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- plugins:js -->
  <script src="<?=site_url('/assets/admin/')?>vendors/js/vendor.bundle.base.js"></script>
  <script src="<?=site_url('/assets/admin/')?>vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?=site_url('/assets/admin/')?>js/off-canvas.js"></script>
  <script src="<?=site_url('/assets/admin/')?>js/hoverable-collapse.js"></script>
  <script src="<?=site_url('/assets/admin/')?>js/misc.js"></script>
  <script src="<?=site_url('/assets/admin/')?>js/settings.js"></script>
  <script src="<?=site_url('/assets/admin/')?>js/todolist.js"></script>
  <script src="<?=site_url('/assets/admin/')?>js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?=site_url('/assets/admin/')?>js/data-table.js"></script>
  <script>
 
 function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });
  </script>
  
  
  <!-- End custom js for this page-->


    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
        <script>
    $(document).ready(function() {
      $('.summernote').summernote({
          height: 500,   //set editable area's height
         
        });

    });
  </script>


</body>


</html>