<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
            Pages List
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Tables</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data table</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Pages List</h4>
              <div class="row">
                <div class="col-12">
                <a href="<?=site_url('Admin/PagesAdd');?>" class="btn btn-outline-primary float-right mb-2">Cerate Page</a>
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Sr No #</th>
                            <th>Page Name</th>
                            <th>Page Link</th>
                         
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                     
                     <?php 
                     $no = 1;
                     foreach($pagesLists as $pagesData)
                     {
                     ?>
                      <tr>
                        <td><?=$no++;?>.</td>
                        <td><?=$pagesData['pageName']?></td>
                        <td><?=site_url('pages').'/'.$pagesData['pageName']?></td>
                        <td><a class="btn btn-primary" href="<?=site_url('Admin/PagesUpdate/').$pagesData['id'];?>">Update</a></td>
                        

                      </tr>
                  <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              </div>
  
        