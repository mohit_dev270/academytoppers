<div class="main-panel">        
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
             
            <?= (!empty($pagesData['pageName']))? 'Page Update':' Page Insert';  ?>
          </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"> Page</a></li>
              
                <li class="breadcrumb-item active" aria-current="page"><?= (!empty($pagesData['pageName']))? 'Course Update':'Course Insert';  ?></li>
                </ol>
            </nav>
          </div>
          <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
              
                  <form class="forms-sample" method="POST" action="" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="exampleInputUsername1">Page Name</label>

                      <input type="text" name="pageName" class="form-control" value="<?= (!empty($pagesData['pageName']))? $pagesData['pageName']:'';  ?>" placeholder="Tittle" required>
                    </div>
                  
                  
                  
                  
                  
                  
                  
                    <div class="form-group">
                      <label for="exampleInputEmail1">Page Content</label>
  
                      <textarea class="summernote form-control" rows="200" cols="100" name="pageContent"><?= (!empty($pagesData['pageContent']))? $pagesData['pageContent']:'';  ?></textarea>
                    </div>
      

                    <button type="submit" name="submit" class="btn btn-primary mr-2">Submit</button>
                  
                  </form>
                </div>
              </div>
            </div>
           
          </div>
        </div>
    