<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
            Student List
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Tables</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data table</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Student List</h4>
              <div class="row">
                <div class="col-12">
               
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Sr No #</th>
                            <th>Student Name</th>
                            <th>Student Email</th>
                            <th>Mobile No.</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                     
                     <?php 
                     $no = 1;
                     foreach($studentList as $student)
                     {
                     ?>
                      <tr>
                        <td><?=$no++;?>.</td>
                        <td><?=$student['firstname']?></td>
                        <td><?=$student['email']?></td>
                        <td><?=$student['mobile']?></td>
                        <td> 
                         <!-- if($student['status'] == '0'){
                            $status = 'InActive';
                        }else{
                            $status = 'Active'; 
                        } -->
                        <!-- <label class="badge badge-success">$status</label></td> -->
                        <label class="badge badge-success"><?= ($student['status'] == 0)? 'Active':'In-Active';  ?></label></td>
                        <!--<td><a href="<?=site_url('Admin/coursesUpdate/').$student['id'];?>" class="btn btn-outline-primary">Edit</a></td>-->
                        

                      </tr>
                  <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              </div>
  
        