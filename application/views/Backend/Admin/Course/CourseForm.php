<div class="main-panel">        
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
             
            <?= (!empty($courseData['tittle']))? 'Course Update':'Course Insert';  ?>
          </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Course</a></li>
              
                <li class="breadcrumb-item active" aria-current="page"><?= (!empty($courseData['tittle']))? 'Course Update':'Course Insert';  ?></li>
                </ol>
            </nav>
          </div>
          <div class="row">
            <div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
              
                  <form class="forms-sample" method="POST" action="" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="exampleInputUsername1">Tittle</label>

                      <input type="text" name="tittle" class="form-control" value="<?= (!empty($courseData['tittle']))? $courseData['tittle']:'';  ?>" placeholder="Tittle" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Short Description</label>
  
                      <textarea class="summernote form-control" name="shortDescription"><?= (!empty($courseData['short_description']))? $courseData['short_description']:'';  ?></textarea>
                    </div>
                    <div class="form-group">
                    <h4 class="card-title">Description</h4>

                    <textarea class="summernote form-control" name="description"><?= (!empty($courseData['description']))? $courseData['description']:'';  ?></textarea>
                  
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Price</label>
                  
                      <input type="text" name="price" class="form-control" value="<?= (!empty($courseData['price']))? $courseData['price']:'';  ?>" placeholder="Price">
                  
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Cover Image</label>
                  
                      <input type="file" id="inputFile" name="cover_image" class="form-control">
                    
                    
                    
                      <img id="image_upload_preview"width="200" height="100" src="<?= (!empty($courseData['cover_image']))?site_url('Images/courseimage/').$courseData['cover_image']:site_url('assets/').'/100x100';  ?>" alt="your image" />
                    
                    

                    </div>

                    <button type="submit" name="submit" class="btn btn-primary mr-2">Submit</button>
                  
                  </form>
                </div>
              </div>
            </div>
           
          </div>
        </div>