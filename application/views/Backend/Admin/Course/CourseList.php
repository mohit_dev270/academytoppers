<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
            Course List
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Tables</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data table</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Course List</h4>
              <div class="row">
                <div class="col-12">
                <a href="<?=site_url('Admin/courseinsert');?>" class="btn btn-outline-primary float-right mb-2">Insert Course</a>
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Sr No #</th>
                            <th>Course Name</th>
                            <th>Price</th>
                            <th>Cover Image</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                     
                     <?php 
                     $no = 1;
                     foreach($courseList as $course)
                     {
                     ?>
                      <tr>
                        <td><?=$no++;?>.</td>
                        <td><?=$course['tittle']?></td>
                        <td><?=$course['price']?></td>
                        <td><img src="<?=site_url('Images/courseimage/').$course['cover_image']?>"></td>
                        <td> <label class="badge badge-success">Active</label></td>
                        <td><a href="<?=site_url('Admin/coursesUpdate/').$course['id'];?>" class="btn btn-outline-primary">Edit</a></td>
                        

                      </tr>
                  <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              </div>
  
        