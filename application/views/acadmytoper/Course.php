<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
	<div class="container-fluid" style="padding-right: 0 ;  padding-left: 0; margin-top: 58px;">
		<div class="large-12 columns">
			<div class="custom1 owl-carousel owl-theme">
				<div class="item">
					<img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner1.png" class="img-fluid">
				</div>
				<div class="item">
					<img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner2.png" class="img-fluid">
				</div>
				<div class="item">
					<img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner3.png" class="img-fluid">
				</div>
				<div class="item">
					<img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner4.png" class="img-fluid">
				</div>
				<div class="item">
					<img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner5.png" class="img-fluid">
				</div>
				<div class="item">
					<img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner6.png" class="img-fluid">
				</div>
				<div class="item">
					<img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner7.png" class="img-fluid">
				</div>
			</div>
		</div>
</section>
<section id="departments" class="departments">
	<div class="container">
		<div class="section-title">
			<h2>Courses</h2>
		</div>
		<div class="row">
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
			<div class="col-lg-3 mt-3 mb-3">
             
                <div class="card p-2">
                      <img src="https://www.dailylearn.in//dlthumb/478x100x344/course_images/84.1620823261.jpg" class="card-img-top border border-2" alt="...">
                    <div class="card-body">
                        <h4>UP Board | Class 11 | Maths</h4>
                        <p>Class 11th - Maths - Syllabus Videos (Including IIT Foundation Foundation) - UP Board </p>
                    </div>

                    <div class="card-footer">
                       <button class="btn btn-danger">Buy</button> 
                       <button  class="btn btn-danger float-end">View</button> 
                    
                    
                    </div>

                </div>
			</div>
	    </div>

</section>