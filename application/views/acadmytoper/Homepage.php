

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="container-fluid" style="padding-right: 0 ;  padding-left: 0; margin-top: 58px;">
      <div class="large-12 columns">
        <div class="custom1 owl-carousel owl-theme">
          <div class="item">
         <img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner1.png" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner2.png" class="img-fluid">

          </div>
          <div class="item">
            <img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner3.png" class="img-fluid">

          </div>
          <div class="item">
            <img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner4.png" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner5.png" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner6.png" class="img-fluid">
          </div>
          <div class="item">
            <img src="<?=site_url('assets/newassets/')?>assets/img/bannerimg/banner7.png" class="img-fluid">
          </div>
        
         
        
        </div>
      </div>
  </section>

  <main id="main">
    <section id="appointment" class="appointment">
      <div class="container">

        <div class="section-title">
          <h2>Talk to our expert
          </h2>
        </div>

        <form action="forms/appointment.php" method="post" role="form" class="php-email-form">
          <div class="row">
            <div class="col-md-4 form-group">
              <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
              <div class="validate"></div>
            </div>
            <div class="col-md-4 form-group mt-3 mt-md-0">
              <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email">
              <div class="validate"></div>
            </div>
            <div class="col-md-4 form-group mt-3 mt-md-0">
              <input type="tel" class="form-control" name="phone" id="phone" placeholder="Your Phone" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
              <div class="validate"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group mt-3">
              <input type="text" name="date" class="form-control datepicker" id="date" placeholder="City" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
              <div class="validate"></div>
            </div>
            <div class="col-md-4 form-group mt-3">
              <select name="department" id="department" class="form-select">
                <option value="">Select Course</option>
                <option value="Department 1">Course 1</option>
                <option value="Department 2">Course 2</option>
                <option value="Department 3">Course 3</option>
              </select>
              <div class="validate"></div>
            </div>
            <div class="col-md-4 form-group mt-3">
              <select name="doctor" id="doctor" class="form-select">
                <option value="">Select State</option>
                <option value="Doctor 1">State 1</option>
                <option value="Doctor 2">State 2</option>
                <option value="Doctor 3">State 3</option>
              </select>
              <div class="validate"></div>
            </div>
          </div>

          <div class="form-group mt-3">
            <textarea class="form-control" name="message" rows="5" placeholder="Message (Optional)"></textarea>
            <div class="validate"></div>
          </div>
          <div class="mb-3">
            <div class="loading">Loading</div>
            <div class="error-message"></div>
            <div class="sent-message">Your appointment request has been sent successfully. Thank you!</div>
          </div>
          <div class="text-center"><button type="submit">Request a Call Back</button></div>
        </form>

      </div>
    </section>
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2>Offline/Online/Residential/Other Courses
          </h2>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <h4 class="font-black">VI to X </h4>
              <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <h4 class="font-black">X </h4>
              <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <h4 class="font-black">XI </h4>
              <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <h4 class="font-black">XII </h4>
              <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <h4 class="font-black">XII Pass    </h4>
              <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <h4 class="font-black">Board </h4>
              <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
            </div>
          </div>

        
        </div>

      </div>
    </section>
    <section id="faq" class="faq section-bg">
      <div class="container">

        <div class="section-title">
          <h2>Latest Courses/Offers
          </h2>
        </div>



        <div class="row">
          <div class="large-12 columns">
            <div class="owl-carousel owl-theme">
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/dummy-img/latest-offer/BITSAT.png" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text text-center">Tablet/PenDrive/SD Card Course for JEE Main  </p>
                  </div>
                  <div class="card-footer text-muted pb-30 text-center">
                    <a href="#" class="btn  btn-xs btn-danger">Enroll Now</a>
                  </div>
                </div>
              </div>
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/dummy-img/latest-offer/CPSAT-Scholarship-Exam.png" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text text-center">Crash Course for JEE MAIN 2021 </p>
                  </div>
                  <div class="card-footer text-muted pb-30 text-center">
                    <a href="#" class="btn  btn-xs btn-danger">Enroll Now</a>
                  </div>
                </div>
              </div>
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/dummy-img/latest-offer/online-video-lecture-library-jee.png" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text text-center">Online Courses for CBSE XII     </p>
                  </div>
                  <div class="card-footer text-muted pb-30 text-center">
                    <a href="#" class="btn  btn-xs btn-danger">Enroll Now</a>
                  </div>
                </div>              </div>
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/dummy-img/latest-offer/online-video-lecture-library-neet.png" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text text-center">Micro Courses for Physics </p>
                  </div>
                  <div class="card-footer text-muted pb-30 text-center">
                    <a href="#" class="btn  btn-xs btn-danger">Enroll Now</a>
                  </div>
                </div>
              </div>
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/dummy-img/latest-offer/recorded-video-lecture-neet.png" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text text-center">CA Foundation

                    </p>
                  </div>
                  <div class="card-footer text-muted pb-30 text-center">
                    <a href="#" class="btn  btn-xs btn-danger">Enroll Now</a>

                  </div>
                </div>
              </div>
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/dummy-img/latest-offer/CPSAT-Scholarship-Exam.png" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text text-center">Live coaching for X+NTSE
                    </p>
                  </div>
                  <div class="card-footer text-muted pb-30 text-center">
                    <a href="#" class="btn  btn-xs btn-danger">Enroll Now</a>
                  </div>
                </div>
              </div>
           
          
            </div>
            </div>





     
      </div>
    </section>
    <section id="departments" class="departments">

    <div class="container">

      <div class="section-title">
        <h2>Coaching Formats</h2>
      </div>
      
      <div class="row">
        <div class="col-lg-3">
        <div class="card card-body">
        <h2 class="card-title"><h4>Classroom Coaching</h4></h2>
        <p class="card-text">One/Two/All subject coaching with full support of coaching system like all subjects Test Series, Doubt Counters, Video Lectures etc.</p>
        <a href="#" class="btn btn-danger waves-effect waves-light" title="Classroom Coaching for NEET, JEE, Pre-Foundation">Learn More</a>
        </div></div>
        <div class="col-lg-3">
        <div class="card card-body">
        <h2 class="card-title"><h4>Residential Coaching</h4></h2>
        <p class="card-text">Residential Schooling with Integrated Coaching for Class VI to XII &amp; XII pass (Stream - PCM, PCB &amp; Commerce) at CP Gurukul Kota. </p>
        <a href="#" class="btn btn-danger waves-effect waves-light" title="Residential Coaching for NEET, JEE, Pre-Foundation">Learn More</a>
        </div></div>
        <div class="col-lg-3">
        <div class="card card-body">
        <h2 class="card-title"><h4>CP Live Classroom</h4></h2>
      <p class="card-text">Kota's expert faculty members provide you live classroom coaching at your home using a simple yet very effective technology.</p>
        <a href="#" class="btn btn-danger waves-effect waves-light" title="CP Live Classroom for NEET, JEE">Learn More</a>
        </div></div>
        <div class="col-lg-3">
        <div class="card card-body">
        <h4 class="card-title">CP eTutor </h4>
      <p class="card-text">Recorded Video Lectures for One/Two or all subjects as per student choice at their home, with all the features of classroom coaching.</p>
        <a href="#" class="btn btn-danger waves-effect waves-light" title="eClassroom (Video Lecture)">Learn More</a>
        </div></div>
       </div>
       <br> <br> <br>
        <div class="section-title">
        <h2>Career Point Coaching System</h2>
      </div>
      
        <div class="row">
          <div class="large-12 columns">
            <div class="owl-carousel owl-theme">
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/career_point_coaching_system/1.jpg" alt="Card image cap">
                  <div class="card-body">
                   <h4>Orientation Session</h4>
                  </div>
            
            
          
            </div>
            </div>
            
                <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/career_point_coaching_system/2.jpg" alt="Card image cap">
                  <div class="card-body">
                   <h4>Orientation Session</h4>
                  </div>
            
            
          
            </div>
            </div>

    <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/career_point_coaching_system/3.jpg" alt="Card image cap">
                  <div class="card-body">
                   <h4>Day Boarding Coaching</h4>
                  </div>
            
            
          
            </div>
            </div>
            
                <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/career_point_coaching_system/4.jpg" alt="Card image cap">
                  <div class="card-body">
                   <h4>Study Material</h4>
                  </div>
            
            
          
            </div>
            </div>

    <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/career_point_coaching_system/1.jpg" alt="Card image cap">
                  <div class="card-body">
                   <h4>Daily Practice Sheets</h4>
                  </div>
            
            
          
            </div>
            </div>

    <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/career_point_coaching_system/5.jpg" alt="Card image cap">
                  <div class="card-body">
                   <h4>Reading Room & Library</h4>
                  </div>
            
            
          
            </div>
            </div>
            
                <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/career_point_coaching_system/6.jpg" alt="Card image cap">
                  <div class="card-body">
                   <h4>Orientation Session</h4>
                  </div>
            
            
          
            </div>
            </div>
                <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/career_point_coaching_system/7.jpg" alt="Card image cap">
                  <div class="card-body">
                   <h4>Periodic Test Series</h4>
                  </div>
            
            
          
            </div>
            </div>

     
      </div>
      </div>
      <br>
      
               <br>

        <div class="section-title">
         <h2>Career Point's Top Rankers at a Glance</h2>
      </div>
      
        <div class="row">
          <div class="large-12 columns">
            <div class="owl-carousel owl-theme">
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/top_ranker/1.png" alt="Card image cap">
                
                </div>
              </div>
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/top_ranker/2.png" alt="Card image cap">
                 
                </div>
              </div>
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/top_ranker/3.png" alt="Card image cap">
                
                </div>              </div>
              <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/top_ranker/4.png" alt="Card image cap">
                
                </div>
              </div>
            <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/top_ranker/5.png" alt="Card image cap">
                
                </div>
              </div>
                  <div class="item m-5">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="<?=site_url('assets/newassets/')?>/assets/top_ranker/6.png" alt="Card image cap">
                
                </div>
              </div>
          
            </div>
            </div>





     
      </div>
      </div>
      </div>

    </div>
  </section>
    
  