<footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>Topers Academy</h3>
            <p>
              A108 Adam Street <br>
              New York, NY 535022<br>
              United States <br><br>
              <strong>Phone:</strong> +1 5589 55488 55<br>
              <strong>Email:</strong> info@example.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Coaching</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">NEET</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">JEE</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">6th-to-10th</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">CA-Foundation</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Borads</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">1-to-1 Live</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">CP Gurukul</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Books</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Scholarship & Rewards</a></li>

              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <!-- <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul> -->
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Join Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

   
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer>

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
<!-- =========================modals====login ============ -->
<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="LoginModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
            <div class="row">
                    <div class="col-sm-6 login-button">
                        <div class="mb-3 mt-1 text-center">
                                         <span>Login</span>  
                         </div>
                     </div>
                     <div class="col-sm-6 signup-button">
                        <div class="mb-3 mt-1 text-center">
                                           <span>Signup</span>     

                        </div>
                     </div>
            
            </div>
            <div class="row mt-2 signup-div">
            
               <form action="<?=site_url('register')?>" method="POST">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="mb-3">
                           <input type="name" name="firstname" class="form-control"  placeholder="First Name">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="mb-3">
                           <input type="last_name" name="lastname" class="form-control"  placeholder="Last Name">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="mb-3">
                           <input type="email" name="email" class="form-control"  placeholder="Email">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="mb-3">
                           <input type="mobile" name="mobile" class="form-control"  placeholder="Mobile">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class="mb-3">
                        <input type="text" name="state" class="form-control"  placeholder="state">
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="mb-3">
                           <input type="text" name="city" class="form-control"  placeholder="city">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="mb-3">
                           <input type="text" name="pincode" class="form-control"  placeholder="Pincode">
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="mb-3">
                           <input type="checkbox" id="accept-terms" name="iagree" required="">
                           I agree to the Terms & Condition
                        </div>
                     </div>
                  </div>
                  <div class="d-grid gap-2">
                     <button class="btn btn-danger" type="submit">CREATE ACCOUNT</button>
                  </div>
               </form>
            </div>
              <div class="row login-div mt-2">
                     <div class="col-sm-12">
                        <div class="mb-3">
                           <input type="Email" class="form-control"  placeholder="Email">
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="mb-3">
                           <input type="password" class="form-control"  placeholder="Last Name">
                        </div>
                     </div>
                     <div class="d-grid gap-2">
                     <button class="btn btn-danger" type="button">LOGIN</button>
                  </div>        
             </div>
         






              </div>
         </div>
      </div>
   </div>
</div>


<!-- Modals error  -->
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="errorModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Error</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <p id="errorMsg"></p>
      </div>
      <div class="modal-footer">
      
      </div>
    </div>
  </div>
</div>
<!-- Modals sucess  -->
<div class="modal fade" id="sucessModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">sucess</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <p id="successMsg"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Understood</button>
      </div>
    </div>
  </div>
</div>
<!-- =========================modals================ -->
  <!-- Vendor JS Files -->
  <script src="<?=site_url('assets/newassets/')?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=site_url('assets/newassets/')?>assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?=site_url('assets/newassets/')?>assets/vendor/php-email-form/validate.js"></script>
  <script src="<?=site_url('assets/newassets/')?>assets/vendor/purecounter/purecounter.js"></script>
  <script src="<?=site_url('assets/newassets/')?>assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
  <!-- Template Main JS File -->
  <script src="<?=site_url('assets/newassets/')?>assets/js/main.js"></script>

</body>

</html>
<script src="<?=site_url('assets/newassets/')?>assets/owlcarousel/vendors/highlight.js"></script>
<script src="<?=site_url('assets/newassets/')?>assets/owlcarousel/js/app.js"></script>
<script>
jQuery(document).ready(function($) {
 
  $('.fadeOut').owlCarousel({
    items: 1,
    animateOut: 'fadeOut',
    loop: true,
    margin: 10,
  });
  $('.custom1').owlCarousel({
    animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    items: 1,
    animateOut: 'fadeOut',
    loop: true,
    autoplay:true,
    autoplayTimeout:2000,
    dots: false,
    
    smartSpeed: 450,
     responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,

  responsive: {
    0: {
      items: 1
    },

    600: {
      items: 1
    },

    1024: {
      items: 1
    },

    1366: {
      items: 1
    }
  }
  });
});
</script>
<style>
  /* Feel free to change duration  */ 
.animated  {
  -webkit-animation-duration : 1000 ms  ;
  animation-duration : 1000 ms  ;
  -webkit-animation-fill-mode : both  ;
  animation-fill-mode : both  ;
}  
/* .owl-animated-out - only for current item */ 
/* This is very important class. Use z-index if you want move Out item above In item */ 
.owl-animated-out {
  z-index : 1 
   }
/* .owl-animated-in - only for upcoming item
/* This is very important class. Use z-index if you want move In item above Out item */ 
.owl-animated-in {
  z-index : 0 
   }
/* .fadeOut is style taken from Animation.css and this is how it looks in owl.carousel.css:  */ 
.fadeOut  {
  -webkit-animation-name : fadeOut  ;
  animation-name : fadeOut  ;
}  


.login-modals{
  
    background: #e42a2a;
    color: #fff;
    margin-top: -17px;
    border-right: 1px solid white;
    padding: 7px;
}
.active-modals-buttons
{
    background: #e42a2a;
    color: #fff;
    margin-top: -17px;
    border-right: 1px solid white;

}
@-webkit-keyframes  fadeOut  {
  0% {
    opacity : 1   ;
  }  
  100% {
    opacity : 0   ;
  }  
}
@keyframes  fadeOut  {
  0% {
    opacity : 1   ;
  }  
  100% {
    opacity : 0   ;
  }  
}
.card-body {
    text-align: center;
}
</style>
<script>
  $(document).ready(function() {
 
 $('.login-button').addClass('login-modals');
 $('.signup-button').removeClass('login-modals');
 $('.login-div').show();
 $('.signup-div').hide();
 $(".login-button").click(function () {
    $('.login-button').addClass('login-modals');
    $('.signup-button').removeClass('login-modals');
    $('.signup-div').hide();
    $('.login-div').show();
   // $(".login-button").css("display", "none");
});
$(".signup-button").click(function () {
     $('.signup-button').addClass('login-modals');
     $('.login-button').removeClass('login-modals');
    $('.login-div').hide();
    $('.signup-div').show();
   // $(".login-button").css("display", "none");
});
 
 
 
 
    var owl = $('.owl-carousel');
    owl.owlCarousel({
      items: 4,
      loop: true,
  
      autoplay: true,
      autoplayTimeout: 3000,
    
       autoplay: true,
  margin: 100,
   /*
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  */
  responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,

  responsive: {
    0: {
      items: 1
    },

    600: {
      items: 3
    },

    1024: {
      items: 4
    },

    1366: {
      items: 4
    }
  }
    });
   
    
  })
  <?php if($this->session->flashdata('successMsg')){ ?>

  $(document).ready(function() {
    $('#sucessModal').modal('toggle');
    $('#sucessModal').modal('show');
    $('#successMsg').text("<?=$this->session->flashdata('successMsg')?>");
  
  
  });
<?php } ?>
  <?php if($this->session->flashdata('error')){ ?>

  $(document).ready(function() {
    $('#errorModal').modal('toggle');
    $('#errorModal').modal('show');
    $('#errorMsg').text("<?=$this->session->flashdata('error')?>");
  
  
  });
<?php } ?>
</script>

