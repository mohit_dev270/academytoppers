<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Topers Academy</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?=site_url('assets/newassets/')?>assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?=site_url('assets/newassets/')?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=site_url('assets/newassets/')?>assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?=site_url('assets/newassets/')?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?=site_url('assets/newassets/')?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="<?=site_url('assets/newassets/')?>assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?=site_url('assets/newassets/')?>assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?=site_url('assets/newassets/')?>assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=site_url('assets/newassets/')?>assets/owlcarousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="<?=site_url('assets/newassets/')?>assets/owlcarousel/assets/owl.theme.default.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!-- Template Main CSS File -->
  <link href="<?=site_url('assets/newassets/')?>assets/css/style.css" rel="stylesheet">

 
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-flex align-items-center fixed-top">
    <div class="container d-flex justify-content-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope"></i> <a href="mailto:contact@example.com">contact@example.com</a>
        <i class="bi bi-phone"></i> +1 5589 55488 55
      </div>

    <div>
    </div>







      <div class="d-none d-lg-flex social-links align-items-center">
        <button type="button" class="btn btn-danger btn-sm">Apply Online  </button> &nbsp;
        <button type="button" class="btn btn-danger btn-sm"> Pay Fee Online  </button> &nbsp;
        <button type="button" class="btn btn-danger btn-sm"> Enquiry  </button> &nbsp;
        










      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <img src="<?=site_url('assets/newassets/')?>assets/logo.png" alt="" class="logo me-auto img-responsive">
      <!-- Uncomment below if you prefer to use an image logo -->
       <!--<a href="index.html" class="logo me-auto"></a>-->
<div class="me-auto"></div>
      <nav id="navbar" class="navbar order-last order-lg-0">
        
        
        <?php 
        $course = '';
        $home = '';
        if($this->uri->segment(1) =='course')
        {
           $course = 'active'; 
        }
        else
        {
            $home = 'active';
        }
        
        
        ?>
        
        <ul>
          <li><a class="nav-link <?=$home?>" href="<?=site_url();?>">Home</a></li>
          <li><a class="nav-link <?=$course?>" href="<?=site_url('course')?>">Course</a></li>
          <?php foreach(getAllCategery() as $categery){ ?>
          <li class="dropdown "><a href=""><span class=""><?=$categery['categeryName'];?></span> <i class="bi bi-chevron-down"></i></a>
          <?php $pagesTittles = getPagesTittlebyId($categery['id']) ?>

        <?php
          if(!empty($pagesTittles))
          {
            echo"<ul>";
            foreach($pagesTittles as $pagesTittle)
            {
 
        ?>
 
 
 
            <li><a href="<?=site_url('informationPages/').$pagesTittle['id'];?>"><?=$pagesTittle['tittle']?></a></li>
 

<?php
            }
            echo"</ul>";
          }
        ?>
    
    
    
         <?php } ?>
          <!-- <ul>
             <li><a href="<?=site_url('/forntend/Product/index/')?>">Categery</a></li>
             <li><a href="<?=site_url('/forntend/Product/index/')?>">Categery</a></li>
             <li><a href="<?=site_url('/forntend/Product/index/')?>">Categery</a></li>
             <li><a href="<?=site_url('/forntend/Product/index/')?>">Categery</a></li>
             <li><a href="<?=site_url('/forntend/Product/index/')?>">Categery</a></li>
         </ul>        
          -->
         </ul> 
          <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#LoginModal">
          Register/Login
        </button>
     
          <!--<li><a class="nav-link scrollto" href="#">Services</a></li>-->
          <!--<li><a class="nav-link scrollto" href="#">Terms of service</a></li>-->
          <!--<li><a class="nav-link scrollto" href="#">Privacy policy</a></li>-->
          <!-- <li><a class="nav-link scrollto" href="#services">Services</a></li>
          <li><a class="nav-link scrollto" href="#departments">Departments</a></li>
          <li><a class="nav-link scrollto" href="#doctors">Doctors</a></li>
          <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li> -->
            <!-- </ul>
          </li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li> -->
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      <!-- <a href="#appointment" class="appointment-btn scrollto"><span class="d-none d-md-inline">Make an</span> Appointment</a> -->

    </div>
  </header><!-- End Header -->
